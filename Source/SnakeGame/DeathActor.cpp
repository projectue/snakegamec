// Fill out your copyright notice in the Description page of Project Settings.

#include "DeathActor.h"
#include "SnakeBase.h"
#include "Interactable.h"

// Sets default values
ADeathActor::ADeathActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ADeathActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADeathActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADeathActor::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto SnakeTwo = Cast<ASnakeBase>(Interactor);
		if (IsValid(SnakeTwo))
		{
			SnakeTwo->Destroy(true, true);
		}
	}

}

