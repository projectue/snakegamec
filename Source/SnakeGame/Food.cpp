
// Fill out your copyright notice in the Description page of Project Settings.

#include "math.h"
#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast <ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			
			float FoodX = FMath::FRandRange(-1000, 1200);
			float FoodY = FMath::FRandRange(-2000, 2000);
			this->SetActorLocation(FVector(FoodX, FoodY, 0));
			
			//Destroy();

		
		}
	}
}
