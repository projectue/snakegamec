// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_DeathActor_generated_h
#error "DeathActor.generated.h already included, missing '#pragma once' in DeathActor.h"
#endif
#define SNAKEGAME_DeathActor_generated_h

#define SnakeGame_Source_SnakeGame_DeathActor_h_16_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_DeathActor_h_16_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_DeathActor_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_DeathActor_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADeathActor(); \
	friend struct Z_Construct_UClass_ADeathActor_Statics; \
public: \
	DECLARE_CLASS(ADeathActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ADeathActor) \
	virtual UObject* _getUObject() const override { return const_cast<ADeathActor*>(this); }


#define SnakeGame_Source_SnakeGame_DeathActor_h_16_INCLASS \
private: \
	static void StaticRegisterNativesADeathActor(); \
	friend struct Z_Construct_UClass_ADeathActor_Statics; \
public: \
	DECLARE_CLASS(ADeathActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ADeathActor) \
	virtual UObject* _getUObject() const override { return const_cast<ADeathActor*>(this); }


#define SnakeGame_Source_SnakeGame_DeathActor_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADeathActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADeathActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeathActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeathActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeathActor(ADeathActor&&); \
	NO_API ADeathActor(const ADeathActor&); \
public:


#define SnakeGame_Source_SnakeGame_DeathActor_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeathActor(ADeathActor&&); \
	NO_API ADeathActor(const ADeathActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeathActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeathActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADeathActor)


#define SnakeGame_Source_SnakeGame_DeathActor_h_16_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_DeathActor_h_13_PROLOG
#define SnakeGame_Source_SnakeGame_DeathActor_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_INCLASS \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_DeathActor_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_DeathActor_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ADeathActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_DeathActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
